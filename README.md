# README for Dogtown-Naxi-Tools & Rules (short: doxi-tools / doxi-rules)

version: 0.4.alpha 

# INTRO

doxi is a distribution of [naxsi](http://naxsi.googlecode.com/)-rules 
that should be an addition to naxsi_core.rules , 
and a set of tools to manage your local nginx/naxsi-installation 
(doxi-rules & doxi-tools).


the rules, ordered in different rulesets, should cover 
configuration or other mistakes, generic web(server|app)-exploits
enumeration-attempts and in, in the long run, upcoming threats 
and exploits, similar to emerging-threats-sigs for snort/suricata

download: 

- doxi-tools: https://bitbucket.org/lazy_dogtown/doxi
- doxi-rules: https://bitbucket.org/lazy_dogtown/doxi-rules

contact: mex <lazy.dogtown@gmail.com>


## About the wording

- when using the word **SIG** we refer to a naxsi-rule/detecting signature
- when using the word **SID** we refer to a rule/signature-ID
- when we talk about a **RULESET** we refer to a file with a list of SIGs
- when using the word **EVENT** we refer to a request that creates a 
hit on one or more SIGs 





# PREREQUISITES

you need to have an nginx-version with naxsi - modules
included, available either via your package-manager
(aptitude install nginx-naxsi) or as source-packages; see
http://naxsi.googlecode.com/ for more details

we assume you have the rights to write into /etc/nginx/
or wherever your nginx-config-files are located; this is needed 
for rules-updates via git/rsync; we assume you use an administrative user
(NOT root) who has sudo-rights to check (nginx -t) and reload nginx 
via /etc/init.d/nginx restart or by a custom command. 
     

# INSTALL

for an inital install:

    $ git clone https://bitbucket.org/lazy_dogtown/doxi.git



please read the installation instructions completely bevore performing
any actions

- copy doxi.conf.template to doxi.conf
- edit doxi.conf and adjust the  given vars to meet your installation; this file is needed and 
read by all tools. 

- execute install.sh -> this will copy the needed directory/files into your nginx_conf_path/doxi-rules/

- copy doxi-rules/rules.conf.template doxi-rules/rules.conf
- edit doxi-rules/rules.conf and choose the ruleset you'd like to load; the rulesets are documented in doxi-rules/README.rulesets
- the file rules.conf will never be overwritten by git-updates.



edit your nginx.conf
  
- add the following lines to your html {} - context you want to protect with naxi:

        # include the subset of loadable rules 
        include     /etc/nginx/doxi-rules/rules.conf;

- add the following lines to your location {} - context

        # loading naxi in learning-mode
        include    /etc/nginx/doxi-rules/learning-mode.rules;
        # OR loading naxi in active-mode
        #include    /etc/nginx/doxi-rules/active-mode.rules;

- if you plan to use your own definitions make sure to have the following additional CheckRules enabled, since this is used by the rulesets; 
not including them means you'll lesser your protection

        # UnWantedAccess 
        CheckRule "$UWA >= 8" BLOCK;
        
        # Identified Attacks
        CheckRule "$ATTACK >= 8" BLOCK;


- if you plan to use whitelists you can use local.rules or create other whitelist-rules-files in doxi-rules/; 
make sure to have them included in location {} - context
    
- finally run the following command to check if nginx would load with the new rulesets

    $ ./dx-update -n && ./dx-update -x 

- generate your whitelists

## changes to a vanilla-naxsi-installation 

we added some more CheckRules for a finer sig-adjustment; make sure you have them included in your configuration:

        # UnWantedAccess 
        CheckRule "$UWA >= 8" BLOCK;
        
        # Identified Attacks
        CheckRule "$ATTACK >= 8" BLOCK;



# doxi.conf

## [global] - section

    
    nginx_conf_path = /etc/nginx
    
    # this command will be called using SUDO
    nginx_bin       = /usr/local/nginx/sbin/nginx
    
    # this command will be called using SUDO
    nginx_restart   = /etc/init.d/nginx restart
    
    # seledct a place to sore your rulesets for you active naxsi
    # usuallay /etc/nginx/doxi-rules;  
    # values might be relative to nginx_conf_path, or give a full path 
    doxi_rules_dir  = doxi-rules 
    rollback_dir    = ./rollback
    stats_dir       = ./stats
    
## [naxsi-ui] - section 

    naxsi_ui_dir   = ../naxsi-ui # path to your naxsi-installation is expected to be there; 
                                 # a valid naxsi-ui.conf MUST exist here (needed for dx-report)

## [masterblaster] - section

- please get used to fabric

~~~

from fabric.api import *
from fabric.contrib.console import confirm

#from fabric.contrib.froxy_lib import *



def rtest():
    run("hostname -f && uptime && uname -a") 

def dupdate_all():
    run("cd $HOME/doxi && ./dx-update -x")

@task
def doxi_update_all():
    """

    """
    host_list = return_hosts(naxsi_server)    
    execute(dupdate_all, hosts = host_list)



~~~

# Tools

## dx-update - Updating Rules

When talking about **updating** you should think about syncing the detection-rules 
with online-repos. you still can edit your local whitelist-rules and sync it 
locally to your naxsi-config-dir



    $ ./dx-update -h

the following files will never be overwritten by updates from git, so you might
edit them locally (although it will be rsynced to your nginx_conf_dir)
    - rules.conf 
    - local.rules
    - any file you create in your doxi-rules / nginx_conf_dir/doxi-rules - dir
      as long as they dont exists in the initial git checkout.
    
for more files that will not be overwritten see .gitignore 

Updates will **ALWAYS** ~~~ git checkout master ~~~ before executing git pull 
for rules-updates, so you might have your local changes in another branch. 


## remote-updates and remote repos

from version 0.3 dx-update is able to include more online-repos 
for rules; a new file- and config-structures was implemented.

to activate a remote repo you need to place a file into repo/your_reponame.repo
with the following content:

~~~

#
# __your_comments__
#

[default]
active = yes
type = git
source = https://bitbucket.org/lazy_dogtown/doxi-rules.git

~~~

the following types are valid:
- git (via http(s) or git - checkout)
- tgz (via http(s) / wget)

the filestructure for the repos will created by the script 
when a new online-repo is detected and looks like this.


~~~
repos/
    -- doxi-rules.repo
    doxi-rules/
        -- scanner.rules
        -- web_server.rules
        -- ...
    local/
    -- rules.conf


~~~

from the repos the files will be collected and synced to 
doxi-rules/ and from there the final update will be done.



## dx-result - Result-Generator

you need to have your doxi-tools setup correctly, please check INSTALL 

this tool is basically used to act as an interface to your naxsi-event-database
and your doxi-rules-installation.

you are able to display certain summaries of events (time/sid-based)
or display infos abour installed rules. for more info on options see

    $ ./dx-result -h 

    

# Workflow - Examples

## installing naxsi/doxi on a site




# Migrate doxi-tools

## from v0.2.x-0.3.x to 0.4

- v0.3.x shouldnt be used, its transition-release to 0.4

if you run a 0.2.x - version, you'll need to add the following section to
doxi.conf [naxsi_ui_dir] is not valid anymore since switching to nx_util, 
starting from naxsi v0.50

~~~

[nx_util]
nx_util_dir   = nx_util 

~~~ 


## from initial upload (before jan 2013) to v0.2

due to changes in rules-management (doxi-rules are now a separate 
git-repo) you need to install doxi again:

- make a backup-copy from your existing doxi-dir
- checkout doxi-tools again
- copy doxi.conf to your new checkout-dir 
- run install.sh
- if you made any changes to any files (like local.rules or other whitelists
you store in doxi-rules) you might want to copy your rulesets back from 
doxi-rules/* to your new doxi-installation
- run ./dx-update -n for a test

there's no migrate-script available. 



# Setup NAXSI for learning-mode

for vals to be included in location{}, see doxi-rules/learning-mode.rules
or simply include this snippets:


~~~
        LearningMode; #Enables learning mode
        SecRulesEnabled;
        #SecRulesDisabled;
        DeniedUrl "/RequestDenied";
        
        
        
        ## check rules
        CheckRule "$SQL >= 8" BLOCK;
        CheckRule "$RFI >= 8" BLOCK;
        CheckRule "$TRAVERSAL >= 4" BLOCK;
        CheckRule "$EVADE >= 4" BLOCK;
        CheckRule "$XSS >= 8" BLOCK;
        
    
    create a location in nginx.conf for the defined DeniedUrl 
    location /RequestDenied {
     proxy_pass http://127.0.0.1:4242;
    }

    create a location to access the webinterface
    location /Naxsi/ {
     proxy_pass http://127.0.0.1:8081;
    }

~~~   
   
- get current naxsi-ui http://code.google.com/p/naxsi/source/checkout
    
- edit naxsi-ui.conf and adjust [nx_intercept] / port, meeting the val from 
~~~    
    location /RequestDenied
    adjust [nx_extract] / port / username and password
    
    run (as non-root) 
    $ nx_intercept.py -c naxsi-ui.conf &
    $ nx_extract.py -c naxsi-ui.conf  &
    
~~~


# Doxi-Modifications to the original naxsi-config:

added more SCORE-targets, like

- $UWA - UnWantedAccess -> requests i definetly DONTWANT and like /w00tw00t.... 
or request for .php / .asp / .cgi on reverse-proxies for app-servers,
or identified web/app/security-scanning bots and tools
these are probably mailicous scanning-requests, so please keep out. 
      
- $ATTACK - identified know attacks that surely should get blocked

# Setup naxsi + fail2ban for blocking the bad guys

- src. http://code.google.com/p/naxsi/wiki/NaxsiFail2ban

~~~    
        /etc/fail2ban/filter.d/nginx-naxsi.conf
        [INCLUDES]
        before = common.conf
        
        [Definition]
        failregex = NAXSI_FMT: ip=<HOST>
        ignoreregex =

    /etc/fail2ban/jail.conf 

        [nginx-naxsi]
        
        enabled = true
        port = http,https
        filter = nginx-naxsi
        logpath = /var/log/nginx/*error.log
        maxretry = 6
~~~

# WHY?
    
    i used to work with IDS/IPS, namely snort+suricata and wrote a lot
    of signatures for those ids as part of the emerging-threats-community
    
    beside this i'm very interested in a standalone WAF, esp. as a part
    of a webserver or as a reverse-proxy. 
    for 2 years now i hoped for ironbee https://www.ironbee.com/ to emerg as a 
    standalone WAF, since mod_security is somewhat uncomfortable to maintain
    in the long run, when operating a variety of different OS.
    
    while exploring the new mod_security-module for nginx i came across NAXSI
    and found that approach of a lighweight webserver-protection very appealing.
    and i like the concept of a location {} - based adjustable WAF. 
    
    so, this is just (as of oct 2012) a POC if nginx+naxsi might be a solution



