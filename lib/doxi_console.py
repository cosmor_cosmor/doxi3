
import memcache, sys, time, hashlib, markdown, re, yaml
import sqlite3 as sdb
import simplejson as json

from werkzeug.security import generate_password_hash, check_password_hash
from flask import flash
from bson.objectid import ObjectId
from sipcalc import *

sys.path.append("lib")

i24   = (86400, "24hrs", "lastday")  # 86400
i7    = (604800, "7days", "lastweek")  # 604800
i30   = (2592000, "30days", "lastmonth") # 2592000
iall  = (315360000, "All", "1970-01-01 00:00:01") # 2592000

stats_value = ["rule_id", "host", "peer_ip", "sensor_id",  ]

#shortcuts for search
scut  = {

    'id'    : 'rule_id',
    'sid'   : 'rule_id',
    'ip'    : 'peer_ip',
    'var'   : 'var_name',
    'mz'    : 'zone'

}

from doxi_lib import *


mc = memcache.Client(['127.0.0.1:11211'], debug=0)
charset = "utf-8"



from doxi_lib import tmpdb, tcreate
from doxi_templates import chart_dashboard_template


from mongoengine import connect, Q


from naxsi_db_schema import Event as nx_event
from naxsi_db_schema import Temp as tmp
from naxsi_db_schema import Stats as stat
from naxsi_db_schema import Messages as msg
from naxsi_db_schema import Agent as agents
from naxsi_db_schema import AgentLog as alog

from pymongo import MongoClient, DESCENDING as DESC


from dx_console import app

mc_key = app.config['SECRET']
mtime = app.config['MEMCACHE_CACHETIME'] 
default_time_range =  app.config['DEFAULT_TIME_RANGE'] 
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

mdb_host = app.config['MONGODB_HOST']
mdb_port = app.config['MONGODB_PORT']
mdb_db   = app.config['MONGODB_DB']
d_limit = app.config["DISPLAY_LIMIT"]
d_data = app.config["DATA_DIR"]
client = MongoClient(host=mdb_host, port=mdb_port)
db = client.naxsi_events
coll_events = db.events
coll_tmp = db.doxi_temp
coll_stats = db.doxi_stats
coll_msgs = db.doxi_messages
coll_agents = db.doxi_agents
coll_alog = db.doxi_agents


if not os.path.isdir(doxi_rules_dir):
    if os.path.isdir("../%s" % doxi_rules_dir):
        doxi_rules_dir = "../%s" % doxi_rules_dir


class PwCheck(object):
    #http://flask.pocoo.org/snippets/54/
    def __init__(self, username, password):
        self.username = username
        self.set_password(password)

    def set_password(self, password):
        self.pw_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)


def agents_run():
    pass 


def agent_log(al_dict):
    ltstamp = t_now()
    ldata = al_dict
    lmsg = al_dict["msg"]
    laid = al_dict["aid"]
    lstatus = al_dict["status"]
    print "> new_agent_log %s" % msg
    alog(tag="agent_log", data = ldata, agent_id =  laid, timestamp = ltstamp, msg = lmsg, status = lstatus).save()       
    


def set_latest_sig():
    satest = nx_event.objects().order_by('-tstamp').limit(1)
    lx = {}
    for l in satest:
        lx["timestamp"] = l.tstamp
        lx["id"] = l.id
        lx["sid"] = l.rule_id
        lx["ip"] = l.peer_ip
        lx["date"] = l.dstamp
        lx["url"] = l.url
        
        
        break 

    #print "setting latest know_sig: %s " % (lx)
    mc.set("%s.latest_sig" % mc_key, (lx))
    return(lx)

def get_latest_sig():
    print "getting latest know_sig: %s :: %s "
    latest_sig = mc.get("%s.latest_sig" % mc_key)
    if latest_sig == None:
        latest_sig = {}
    return(latest_sig)


def rep_check(ip):
    """
    takes ip/cidr from alerts and checks for occurence in 
    this is done from robot
    
    0           - OK
    1+          - IP/CIDR FOUND (count)
    
    "nonono"    - misc error
    
    """
    
    ret = 0
    
    if ip == "all":
        # execution from console
        # ips in an array
        peer_ip_u = coll_stats.find({"tag": "peer_ip_list"})
        cip = []
        for h in peer_ip_u:
            #print "sensorinfo for %s" % s
            #print s["data"]
            hd = json.loads(h["data"])
            
            for d in hd:
                print "   h: %s" % d
                cip.append(d) 
            break
    
    else:
        # single ip array
        cip = [ip]    


    mips  = {} # my ips
    mcidr = {} # my cidrs
    found = {}
    ip_list = "%s/ip-reputation.list" % d_data
    fip = open(ip_list).readlines()
    for ix in fip:
        ia = ix.strip()
        if len(ia) < 4:
            continue
        elif ia[0] == "#":
            continue
        else:
            ig = ia.split(":")[0].strip()
            
            try:
                im = ia.split(":")[1].strip()
            except:
                im = "unknown"
            if ig.find("/") > -1:
                try:
                    net_ok = Network(ig)
                    mcidr[ig] = im
                except:
                    flash("wrong cidr: %s" % ig)
                    continue
            else:
                try:
                    i_ok = socket.gethostbyname(ig)
                    if i_ok == ig:
                        mips[ig] = im
                    else:
                        flash("wrong ip: %s" % ig)
                        continue
                except:
                    flash("error on ip-check ip: %s" % ig)
                    continue

                        


    for ix in cip:
        print "> rep_check(%s)" % ix
        
        try:
            sip = socket.gethostbyname(ix)
        except:
            print "> cannot extract ip/gethostbyname from: %s" % ip
            return("nonono")

        if sip != ix: # its not an ip!!
            print "given ip(%s) converted(%s) is NOT an IP???" % (ip, sip)
            return("nonono")
        
        if sip in mips:
            c24 = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - 86400)).count()
            c7  = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - (86400 * 7))).count()
            c30 = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - (86400 * 30))).count()
            found[sip] = (mips[sip], c24, c7, c30)

            flash("!!!!!!!!!!!! %s seems to be known (ipcheck) : %s" % (sip, mips[sip]))
            ret += 1
        
        for cdr in mcidr:
            nt = Network(cdr)
            if nt.in_network(sip):
                c24 = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - 86400)).count()
                c7  = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - (86400 * 7))).count()
                c30 = nx_event.objects(peer_ip=sip, tstamp__gt=int(t_now() - (86400 * 30))).count()
                found[sip] = (mcidr[cdr], c24, c7, c30)
                flash("!!!!!!!!!!!! %s seems to be known (cidr_check) : %s" % (sip, mcidr[cdr]))
                ret += 1
        
        
    return(found)
        
def range_check(range_name):
    
    print "> range_check: %s" % range_name
    g_range = range_name
    tn = "1d"
    t_range = 86400
    if g_range.find("h") > -1:
        print "  > range-> h"
        # assuming range=Xh -> meaning x hours
        g_r = g_range.split("h")[0]
        t_range = int(g_r) * 3600
        tn = "%sh" % g_r

    elif g_range.find("d") > -1:
        print "  > range-> d"
        # assuming range=Xhd -> meaning x days
        g_r = g_range.split("d")[0]
        print "g_r: %s" % g_r
        t_range = int(g_r) * (86400)
        print "t_r: %s" % t_range
        
        tn = "%sd" % g_r


    elif g_range.find("w") > -1:
        print "  > range-> w"
        tstep = 604800
        # assuming range=Xw -> meaning x weeks
        g_r = g_range.split("w")[0]
        t_range = int(g_r) * (86400 * 7)
        tn = "%sd" % g_r


    elif g_range.find("m") > -1:
        print "  > range-> m"
        ts = 604800 # weeks, NOT months

        # assuming range=Xm -> meaning x months
        g_r = g_range.split("m")[0]
        t_range = int(g_r) * (3600 * 24 * 30)
        tn = "%sm" % g_r
            

    print "range_exporting: %s : %s" % (t_range, tn)
    return(t_range, tn)

def check_latest_sig(check_id):
    latest_tstamp, latest_id = get_latest_sig()
    newer = nx_event.objects(tstamp_gt=self.user[0]).order_by('-tstamp')
    
    an = {}
    for l in newer:
        ns = {}
        ns["id"] = l.rule_id
        ns["ip"] = l.peer_ip
        ns["date"] = l.dstamp
        ns["url"]  = l.url

#~ def update_uniq_vals(key, val):
#~ 
    #~ 
    #~ #test for emtpy stats
    #~ tsensors = stat(tag="sensor_id_list").find().count()        
    #~ if tsensors < 1:
        #~ print "> sems like we have to generate uniq_stats from scratch \n> this might take a littel while"
        #~ generate_uniq_stats_info()
    

def check_uniq_stats(event):
#    print "> uniq_check  %s" % event 

    def check_val(key, val):
        tuniq = stat.objects(tag="%s_list" % key)
        if len(tuniq) == 0:
            generate_uniq_stats_info()
        tuniq = stat.objects(tag="%s_list" % key)
        td = {}
        for t in tuniq:
            td = json.loads(t.data)
            ti = t.id
            break
        if len(td) == 0:
            generate_uniq_stats_info()
    
            
        if val in td:
            #~ print "> KNOWN uniq_check %s :: %s " % (key, val)
            return(1)
        else:
            td[val] = "found: %s" % time.strftime("%Y-%m-%d %H:%M:%S")
            #~ print "> new uniq_check %s :: %s " % (key, val)
            ts = stat.objects(id=ti).update(set__data = json.dumps(td))
            return(0)

    

    res = {} 
    res_m = {}
    cid = coll_events.find({"_id" : ObjectId(event)}).count()
    if cid < 1:
        print "[-] NOT FOUND ERRO: cannot uniq_check event: %s" % event 
        return(res)
    cid = coll_events.find({"_id" : ObjectId(event)})
    
    for c in cid:
        if check_val("url", c["url"]) == 0:
            res_m["url"] = "new url found: %s" % c["url"]
        if check_val("peer_ip", c["peer_ip"]) == 0:
            res_m["peer_ip"] = "new ip found: %s" % c["peer_ip"]
        if check_val("host", c["host"]) == 0:
            res_m["host"] = "new host found: %s" % c["host"]
        if check_val("rule_id", str(c["rule_id"])) == 0:
            res["rule_id"] = "new id found: %s" % c["rule_id"]
        if check_val("var_name", c["var_name"]) == 0:
            res_m["var_name"] = "new var_name found: %s" % c["var_name"]
        if check_val("sensor_id", c["sensor_id"]) == 0:
            res_m["sensor"] = "new sensor found: %s" % c["sensor_id"]

    if len(res_m) < 1:
        #~ print "[+] nothing new found for %s" % event 
        return(res)
        
    nms_msg = ""
    for m in res_m:
        nms_msg += "[+] %s \n" % res_m[m] 

    mmsg = {}
    mmsg["msg"] = nms_msg
    mmsg["host"] = c["host"]
    mmsg["peer_ip"] = c["peer_ip"]
    mmsg["url"] = c["url"]
    mmsg["rule_id"] = c["rule_id"]
    mmsg["var_name"] = c["var_name"]
    mmsg["sensor_id"] = c["sensor_id"]
    mmsg["tstamp"] = c["tstamp"]
    
    msg_new_event(mmsg)
    
    return(mmsg)


def update_user_msgs(user, stuff_id):

    print "> updating user_msg_list -> %s | %s" % (user, stuff_id)

    utag = "%s_msgs_list" % user
    umsg = coll_msgs.find({"tag" : "%s" % utag}).count()
    if umsg < 1:
        print "> init_set user_msg_list"
        msg(tag=utag, data = [stuff_id]).save()
    else:
        nu = []
        umsg = coll_msgs.find({"tag" : "%s" % utag})
        for m in umsg:
            nu = m["data"]
            break 
        nu.append(stuff_id)
        print "> appending user_msg_list"
        ts = msg.objects(tag=utag).update(set__data = nu)
    
    flash("updated user_msgs_list :: %s" % stuff_id[0:32])
    return()
    
def delete_uniq_data():
    print "> dropping uniq_stats"
    stat.drop_collection()
    generate_uniq_stats_info()

def generate_uniq_stats_info():

    print "> generating host/sensor-infos"
    for ku in ["sensor_id"]:
        all_xxx = {}
        all_s = nx_event.objects().distinct(ku)
        for s in all_s:
            shx = []
            sh = coll_events.find({"%s" % ku: s}).distinct("host")
            for h in sh:
                shx.append(h)
                
            all_xxx[s] = shx
        flash("[+] init: added %s %s to uniq_stats" % (len(all_xxx), ku) )
        msg_admin("[+] init: added %s %s to uniq_stats" % (len(all_xxx), ku) )
        tsensors = stat(tag="%s_list" % ku, data = json.dumps(all_xxx)).save()        
        print "> added %s %ss" % (len(all_xxx), ku)
    
    #~ all_hosts   = {}            
    #~ all_h = nx_event.objects().distinct("host")
    #~ for s in all_h:
        #~ hx = []
        #~ sh = nx_event.objects(host=s).distinct("sensor")
        #~ for h in sh:
            #~ hx.append(h)
            #~ 
        #~ all_hosts[s] = hx
    #~ thosts = stat(tag="hostlist", data = json.dumps(all_hosts)).save()
    #~ print "> added %s hosts" % len(all_hosts)

    for ko in ["host", "url", "rule_id", "peer_ip", "var_name"]:
        print "> generating uniq_stats for %s" % ko
        all_ux   = {}            
        all_u = nx_event.objects().distinct(ko)
        for s in all_u:
            hx = []
            sh = coll_events.find({"%s" : s}).count()
            all_ux[s] = sh
        tx = stat(tag="%s_list" % ko, data = json.dumps(all_ux)).save()
        flash("[+] init: added %s %s to uniq_stats" % (len(all_ux), ko) )
        msg_admin("[+] init: added %s %s to uniq_stats" % (len(all_ux), ko) )
        print "> added %s %s" % (len(all_ux), ko )


def t_now():
    return(int(time.time()))


def msg_admin(message, area = "console"):
    msgx = {}
    msgx["tstamp"] = t_now()
    msgx["msg"]    = message
    msgx["area"]   = area
    tstamp = t_now()
    msg(tag="admin_messages", data = msgx, tstamp = tstamp).save()        


def msg_new_event(message):
    tn = t_now()
    print "> new_event_msg %s" % tn
    msg(tag="new_event", data = message, tstamp = tn).save()        



def msg_agent(am_dict):
    tn  = t_now()
    mdata    = am_dict
    mmsg     = am_dict["msg"].encode(charset)
    maid     = am_dict["aid"].encode(charset)
    print "> new_agent_msg %s" % mmsg

    msg(tag="agent_message", data = mdata, aid =  maid, tstamp = tn).save()        
    

def generate_userdicts():

    print "> generating users + logins"

    users = {} 
    
    for ufile in glob.glob("data/users/*"):
        if os.path.isdir(ufile):
            print "> isdir: %s" % ufile
            continue
        user = {}
        print "  > checking %s " % ufile
        user["name"] = ufile.split("/")[-1]

        try:
            uinfo = yaml.load(file('%s' % ufile, 'r'))
            user["pass"]    = uinfo['password']
            user["sensors"] = uinfo['sensors']
            user["email"]   = uinfo['email']
            user["hosts"]   = uinfo['hosts']
            user["active"]  = uinfo['active']
            user["email"]   = uinfo['email']
            user["uid"]      = uinfo['uid']

            try:
                user["admin"]   = int(uinfo['admin'])
            except:
                user["admin"]  = 0
        except:
            print "  [-] error while trying to check %s " % ufile
            continue
        
        
        if int(user["active"]) != 1:
            print "  [-] user not active %s " % user["name"]
            continue

        users[user["name"]] = user 
        print "[+] added %s to users_dict" % user 
        
    
    mc.set("%s.users" % mc_key, "%s" % json.dumps(users))


def generate_results():

    # for dashboard
    st = int(time.time())
    mc_key = app.config['SECRET']
    mtime = app.config['MEMCACHE_CACHETIME'] 
    mc = memcache.Client(['127.0.0.1:11211'], debug=True)
    
    from doxi_lib import tmpdb, tcreate
    from doxi_templates import chart_dashboard_template


    from mongoengine import connect, Q
    sys.path.append("lib")

    from naxsi_db_schema import Event as nx_event
    from naxsi_db_schema import Temp as tmp

    from pymongo import MongoClient, DESCENDING as DESC
    client = MongoClient(host=mdb_host, port=mdb_port)
    db = client.naxsi_events
    coll_events = db.events
    coll_tmp = db.doxi_temp

    
    print "> dropping temporary_dbs"
    tmp.drop_collection()
    connect("naxsi_events")

    print "> reading events"
    #~ all_ips = nx_event.objects().distinct("peer_ip")
    #~ all_sids = nx_event.objects().distinct("rule_id")
    ip_tc = {}


    tnow = t_now()

    print "> generating dashboard - stats"

    
    tdr = ["d", "w", "m"]
    for times in tdr:
        timen = time_ranges[times][1]  # name
        timet = time_ranges[times][0]  # offset for tnow - offset / steps for charts
        timex = time_ranges[times][3]  # how many chart_steps

        #print "  > %s " % timen 
        sel_dat = int(tnow - timet)


        
        for stat in stats_value:

            c_for = stat

            

            

            print "  > generating - stats - %s" % c_for.upper()
            ctemp = sdb.connect(tmpdb)
            ct = ctemp.cursor()
            ct.execute(tcreate)
            ctemp.commit()
    
            all_ev = nx_event.objects(tstamp__gte=sel_dat).distinct(c_for)
            for i in all_ev:
                #print "   %s: %s" % (c_for, i)
                sel = {}
                sel["%s" % c_for] = { "$in": [i] }
                sel["tstamp"] = { "$gte": sel_dat }
                i_c = coll_events.find(sel).count()
                if i_c == 0:
                    print "  -- skipping %s - nocount" % s
                    continue
                ct = ctemp.cursor()
                tins = "insert into tmptbl (count, sid) values ('%s', '%s')  " % (i_c, i)        
        
                ct.execute(tins)
        
            # sorted stats for hosts
            ct = ctemp.cursor()
            s_sel = "select sid, count from tmptbl order by count DESC"
            cx = ct.execute(s_sel)
            cr = cx.fetchall()
            #print cr
            t_tc  = tmp(tag="dash_%s_%s" % (c_for, timen), data=cr ).save()
            print "  > inserted ( %s ) %s  to %s" % (c_for, len(cr), timen)
            ctemp.close()
            
            
            sel = {} 
            
            
            c_data = get_chart_data(c_for, timex, timet, times)
            
            #print c_data

            chart_tag = "dash_chart_%s_%s" % (timen, c_for )
            
            if c_for in ["rule_id"]:
                c_type = "spline"
            else:
                c_type = "column"
            
            chart_data = chart_dashboard_template % (chart_tag, c_type, "%s - %s Charts" % (timen, c_for), c_for,  json.dumps(c_data[1]), json.dumps(c_data[0]) )
            t_chart  = tmp(tag="%s" % (chart_tag), data=chart_data ).save()
            print "  > saved: %s" % (chart_tag)
            #print c_data

    
    
    
    # generate user_dicts
    generate_userdicts()
    
    set_latest_sig()

    
            
 
        #~ print "  > generating HOST/SENSOR - stats"
        #~ ctemp = db.connect(tmpdb)
        #~ ct = ctemp.cursor()
        #~ ct.execute(tcreate)
        #~ ctemp.commit()
#~ 
        #~ all_hosts = nx_event.objects(tstamp__gte=sel_dat,).distinct("host")
        #~ for i in all_hosts:
            #~ print "host: %s" % i
            #~ i_c = nx_event.objects(tstamp__gt=sel_dat, host = i).count()
            #~ if i_c == 0:
                #~ #print "  -- skipping %s - nocount" % s
                #~ continue
            #~ ct = ctemp.cursor()
            #~ tins = "insert into tmptbl (count, sid) values ('%s', '%s')  " % (i_c, i)        
    #~ 
            #~ ct.execute(tins)
    #~ 
        #~ # sorted stats for hosts
        #~ ct = ctemp.cursor()
        #~ s_sel = "select sid, count from tmptbl order by count DESC"
        #~ cx = ct.execute(s_sel)
        #~ cr = cx.fetchall()
        #~ t_tc  = tmp(tag="dash_host_%s" % timen, data=cr ).save()
        #~ print "  > inserted ( %s ) hosts  to %s" % (len(cr), timen)
        #~ ctemp.close()
#~ 
#~ 
        #~ ctemp = db.connect(tmpdb)
        #~ ct = ctemp.cursor()
        #~ ct.execute(tcreate)
        #~ ctemp.commit()
#~ 
        #~ all_sensors = nx_event.objects().distinct("sensor_id")
        #~ for i in all_sensors:
            #~ i_c = nx_event.objects(tstamp__gt=sel_dat, sensor_id = i).count()
            #~ if i_c == 0:
                #~ #print "  -- skipping %s - nocount" % s
                #~ continue
            #~ ct = ctemp.cursor()
            #~ tins = "insert into tmptbl (count, sid) values ('%s', '%s')  " % (i_c, i)        
    #~ 
            #~ ct.execute(tins)
    #~ 
        #~ # sorted stats for ids
        #~ ct = ctemp.cursor()
        #~ s_sel = "select sid, count from tmptbl order by count DESC"
        #~ cx = ct.execute(s_sel)
        #~ cr = cx.fetchall()
        #~ t_tc  = tmp(tag="dash_sensor_%s" % timen, data=cr ).save()
        #~ print "  > inserted ( %s ) sensors  to %s" % (len(cr), timen)
        #~ ctemp.close()
#~ 
#~ 
        #~ 
        #~ # generating sigs-stats
        #~ ctemp = db.connect(tmpdb)
        #~ ct = ctemp.cursor()
        #~ ct.execute(tcreate)
        #~ ctemp.commit()
        #~ print "generating sigs-stats"
        #~ all_sids = nx_event.objects().distinct("rule_id")
        #~ for s in all_sids:
            #~ s_c = nx_event.objects(tstamp__gt=sel_dat, rule_id = s).count()
            #~ if s_c == 0:
                #~ #print "  -- skipping %s - nocount" % s
                #~ continue
            #~ ct = ctemp.cursor()
            #~ tins = "insert into tmptbl (count, sid) values ('%s', '%s')  " % (s_c, s)        
#~ 
            #~ ct.execute(tins)
#~ 
        #~ # sorted stats for ids
        #~ ct = ctemp.cursor()
        #~ s_sel = "select sid, count from tmptbl order by count DESC"
        #~ cx = ct.execute(s_sel)
        #~ cr = cx.fetchall()
        #~ t_tc  = tmp(tag="dash_sig_%s" % timen, data=cr ).save()
        #~ print "  > inserted ( %s ) sigs to %s" % (len(cr), timen)
        #~ ctemp.close()
#~ 
#~ 
        #~ # generating ip-stats
        #~ ctemp = db.connect(tmpdb)
        #~ ct = ctemp.cursor()
        #~ ct.execute(tcreate)
        #~ ctemp.commit()
        #~ print "generating ip-stats"
        #~ all_ips = nx_event.objects().distinct("peer_ip")
        #~ for i in all_ips:
            #~ i_c = nx_event.objects(tstamp__gt=sel_dat, peer_ip = i).count()
            #~ if i_c == 0:
                #~ #print "  -- skipping %s - nocount" % s
                #~ continue
            #~ ct = ctemp.cursor()
            #~ tins = "insert into tmptbl (count, sid) values ('%s', '%s')  " % (i_c, i)        
#~ 
            #~ ct.execute(tins)
#~ 
        #~ # sorted stats for ids
        #~ ct = ctemp.cursor()
        #~ s_sel = "select sid, count from tmptbl order by count DESC"
        #~ cx = ct.execute(s_sel)
        #~ cr = cx.fetchall()
        #~ t_tc  = tmp(tag="dash_ip_%s" % timen, data=cr ).save()
        #~ print "  > inserted ( %s ) ips  to %s" % (len(cr), timen)
        #~ ctemp.close()


    # its only a test
    
    #~ print "> generating per_ip - stats :: %s ips" % len(all_ips)
    #~ for ip in all_ips:
        #~ ip_list = {}
        #~ print "  > checking ip: %s" % ip
        #~ frst      = nx_event.objects.filter(peer_ip=ip).order_by('tstamp').limit(1)
        #~ last      = nx_event.objects.filter(peer_ip=ip).order_by('-tstamp').limit(1)
        #~ ipx        = nx_event.objects.filter(peer_ip=ip).order_by('tstamp')
        #~ print "    %s events" % len(ipx) 
        #~ ip_count = len(ipx)
        #~ ip_first = frst[0].dstamp
        #~ ip_last  = last[0].dstamp
        #~ ip_all  =  ipx
        #~ ip_list["data"] = (ip, ip_count, ip_first, ip_last, ip_all)
#~ 
        #~ t_rex = tmp(tag=ip, tdata=ip_list ).save()
        #~ t_tc  = tmp(tag="countset", ip=ip, count = ip_count).save()
#~ 
        #~ try:
            #~ t_rex = tmp(tag=ip, tdata=ip_list ).save()
            #~ t_tc  = tmp(tag="countset", ip=ip, count = ip_count).save()
        #~ except:
            #~ print "cannot insert tap_ip::%s" % (ip)
            #~ continue
#~ 
    #~ sys.exit()

    mc.set("%s.active" % mc_key, "yezzz!", mtime)
    et = int(time.time())

def url_convert(mal_url):
    c_url = ""
    for c in mal_url:        
        try:
            ca = c.encode(charset)
            c_url += ca
        except:
            continue
    return(c_url)


def mgr_check_latest():
    # check for new sigs 
    set_latest_sig()
    lus = (si["latest_sig"])
    app.jinja_env.globals['latest_user_sig'] = lus
    lss = get_latest_sig()
    if lus < lss["timestamp"]:
        print "oups, new events found"
        ns = nx_event.objects(tstamp__gt=lus).count()
        flash("""%s new sigs!!!""" % ns)
        app.jinja_env.globals['new_events'] = ns
        nc = nx_event.objects(tstamp__gt=lus)
        for c in nc:
            nms = {}

            if check_uniq_stats("url", c.url) == 0:
                nms["url"] = "new url found: %s" % c.url
            if check_uniq_stats("peer_ip", c.peer_ip) == 0:
                nms["peer_ip"] = "new ip found: %s" % c.peer_ip
            if check_uniq_stats("host", c.host) == 0:
                nms["host"] = "new host found: %s" % c.host
            if check_uniq_stats("rule_id", str(c.rule_id)) == 0:
                nms["rule_id"] = "new id found: %s" % c.rule_id
            if check_uniq_stats("var_name", c.var_name) == 0:
                nms["var_name"] = "new var_name found: %s" % c.var_name
            if check_uniq_stats("sensor_id", c.sensor_id) == 0:
                nms["sensor"] = "new sensor found: %s" % c.sensor_id
            
            if len(nms) > 0:
                nms_msg = ""
                for n in nms:
                    nms_msg +="[+] %-12s : %s\n" % (n, nms[n])
                mmsg = {}
                mmsg["msg"] = nms_msg
                mmsg["host"] = c.host
                mmsg["peer_ip"] = c.peer_ip
                mmsg["url"] = c.url
                mmsg["rule_id"] = c.rule_id
                mmsg["var_name"] = c.var_name
                mmsg["sensor_id"] = c.sensor_id
                mmsg["tstamp"] = c.tstamp
                flash("%s " % nms_msg)
                msg_new_event(mmsg)
            
            
        
        


def get_sensorinfos(sensor = 0):
    
    sd = {}
    sensors = coll_stats.find({ "tag": "sensor_id_list" })
    for s in sensors:
        #print "sensorinfo for %s" % s
        #print s["data"]
        sd = json.loads(s["data"]) 
    if sensor != 0:
        if sensor in sd:
            sds = {}
            sds[sensor] = sd[sensor]
            sd = sds
    return(sd)
    
    

def get_peerip_list():
    
    pl = []
    peers = coll_stats.find({ "tag": "peer_ip_list" })
    for i in peers:
        ij = json.loads(i["data"])
        
        for d in ij:
            print "   h: %s" % d
            pl.append(d) 
        break

    return(pl)


def get_hostlist(sensor = 0):
    
    ha = []
    hosts = coll_stats.find({ "tag": "host_list" })
    for h in hosts:
        #print "sensorinfo for %s" % s
        #print s["data"]
        hd = json.loads(h["data"])
        print hd
        
        for d in hd:
            print "   h: %s" % d
            ha.append(d) 
        break
    return(ha)

def get_user_agents(uid):
    res = []

    active_agents = {}
    s_a = agents.objects(user_id=uid, tag="agent").order_by("-next_run", "-timestamp")
    for a in s_a:
        res.append(a.agent_id)


    return(res)


def get_chart_data(search, rangex, step, times):
    
    val = []
    desc = []
    sel = {}
    tnow = t_now()
    for c_step in reversed(range(rangex)):
        start = tnow - ((c_step+1) * step) 
        stop  = tnow - (c_step * step)  
        #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        
        # getting descriptor
        dk = "%" + times
        if times == "d":
            dk = "%m-%" + times
        elif times == "w":
            dk = "%V"
        
        t_d = time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        sel["tstamp"] = { "$gte": start , "$lt": stop }
        if search ==  "rule_id":
            res = db.events.find(sel).count()
        else:
            res = db.events.find(sel).distinct(search)
            l = 0
            for e in res:
                l +=1
            res = l
        
        val.append(res)
        desc.append(t_d)
    
    return(val, desc)

def event_search(srch, time_range = "", skip = 0, cdata = 1 ):
    """
    
    res, c_data, u_ip, u_sid, u_host = event_search(srch, time_range = t_range, skip = skip, cdata = 1 )
    
    takes
        srch    -> search-term
        t_range -> time range in words, e.g. 7d
        skip    -> how many steps to skip for pagination
        cdata   -> generate additional data, defaults to 1, turn off for whitelist
    gives:
    
        res     -> result
        c_data  -> chart_data
        u_*     -> uniq_stuff 
    
    """
    
    sigs_dict = sigs_parse(glob.glob("%s/*.rules" % doxi_rules_dir))

    from pymongo import MongoClient, DESCENDING as DESC
    client = MongoClient(host=mdb_host, port=mdb_port)
    db = client.naxsi_events
    coll = db.events

    sa = {}
    print "searching: %s" % srch

    # extracting time-ranges
    
    if len(time_range) < 2:
        time_range = default_time_range

    tr = {}
    start = stop = None
    if time_range.find("-") > -1:
        start, stop = time_range.split("-")
    
    else:
        start = "0"
        stop = time_range 

    tr["start"] = start 
    tr["stop"]  = stop
    tnow = t_now()

    tf = {}
    tf["start"] = 0
    tf["stop"] = 86400

    
    for x in tr:
        print x, tr[x]
        for y in time_ranges:
            if tr[x].find(y) > -1:
                print "found timerange: %s" % y 
                print time_ranges
                step = time_ranges[y][0]
                try:
                    ctep = int(tr[x].replace(y, ""))
                except:
                    ctep = 1
                back_range = ctep * step
                tf[x] = back_range
                print "backrange:"
                print x
                print tr
                print tf
                print ctep
                break
            
    print "tf:"
    print tf
    
    

    smaller = tnow - tf["start"]
    bigger  =  tnow - tf["stop"]
    sa["tstamp"] = { "$lte": smaller, "$gt": bigger  }


    # extracting search_terms
    
    s1 = ""
    done = 0
    ov = 0
    ox = 0
    op = {}
    sep = ["&"] # separators for search-rules
    ops = ["-", "!"] # operators 
    mag = ["|"] # magic operator for stuff like url | uniq
    mag_allowed = ["uniq"]
    vals = ["rule_id", "peer_ip", "var_name", "var", "url", "zone", "host", "ip", "sid", "id", "mz", "sensor_id"]
    

    if len(srch.split("|")) > 1:
        ox, ov = srch.split("|")
        ov = ov.strip()
        ox = ox.strip()
        if ox not in mag_allowed or ov not in vals:
            ov = ox = 0
            print "unknown [mag] in %s " % srch

        else:
            if ov in scut:
                ov = scut[ov]

            res = ["ssearch", srch, ov]

            c_data = []
            if ox == "uniq":
                for cd in coll.find(sa).distinct(ov):
                    so = {}
                    for v in sa:
                        so[v] = sa[v]
                    so[ov] = { "$in": [cd]}
                    cdc = coll.find(so).count()
                    c_data.append([cd, cdc])
            
            return(res, c_data, [], [], [])
    if not srch.find("=") > -1:
        return([], [[], []], [], [])
    for c in srch:
        if c not in sep:
            s1 += c
            continue
        else:
            print "operator_checking s1: %s [ %s ]" % (s1, c)
            sv = s1.split("=")
            sv0 = sv[0].strip().encode(charset)
            sv1 = sv[1].strip().split(",")
            print "sv1: "
            print sv1
            #~ try:
                #~ sv1 = sv1.encode(charset)
            #~ except:
                #~ pass
            #~ try:
                #~ sv1 = int(sv1)
            #~ except:
                #~ pass 
        
            if len(sv) < 2:
                print "1.cannot extract stuff from srch_pharse: %s [ %s ]" % (s1, srch)
                continue
            else:
                if sv0 in vals:
                    v = sv0
                    print "v : %s" % v
                    if v in scut:
                        v = scut[v]
                    for s in sv1:
                        print "adding %s to v : %s" % (s, v)
                        c_rule = "$in"
                        s = s.strip()
                        if s[0] in ops:
                            if s[0] in ["!", "-"]:
                                c_rule = "$nin"
                            s = s[1:]
                        try:
                            s = s.encode(charset)
                        except:
                            pass 
        
                        
        
                        # doing regex-search 
                        if s.find("*") > -1:
                            s = s.replace(".", "\.").replace("*", ".*")
                            c_rule = "$regex"
                            print "regex on %s" % s
                            sr = re.compile(s)
                            if v in sa:
                                if c_rule in sa[v]:
                                    print "known regex/known v: %s / %s" % (v, s)
                                    sa[v] = { "$all" : [sa[v][c_rule], sr], "$options": 'i'}
                                else:
                                    print "new regex/know v: %s / %s" % (v, s)
                                    sa[v] = { "%s" % c_rule: sr,  "$options": 'i'}
                                    print sa[v][c_rule]
                                    
                            else:
                                print "new regex/new v: %s / %s" % (v, s)
                                sa[v] = { "%s" % c_rule: sr,  "$options": 'i'}
                                print sa[v][c_rule]
                            
                            continue
        
                        try:
                            s = int(s)
                        except:
                            pass 
        
        
                        
                        if v in ["url", "zone"]:
                            c_rule = "$regex"
                            sa[v] = { "%s" % c_rule: "%s" % s.replace(".", "\.").replace("*", ".*"),  "$options": 'i'}
                            continue
                        if v in sa:
                            print "2.v in sa: %s" % v
                            try:
                                print "c_rule is known, appending: %s" % s
                                f = sa[v][c_rule]
                                print f
                                f.append(s)
                                print f
                            except:
                                print "c_rule is new: %s" % c_rule
                                f = [s]
                            
                            sa[v][c_rule] = f
                        else:
                            print "new v in sa: %s / %s" % (v, s)
        
        #                        sa[v][c_rule] = [s]
                            sa[v] = { "%s" % c_rule: [s] }
        
                        print "added rule_id to sa: %s" % s
                        print sa
                else:
                    print "unknown search-term: %s" % sv[0]
                    sa = {}
                
            
            s1 = ""


    print sa
    # final step
    print "final s1: %s " % s1
    sv = s1.split("=")
    sv0 = sv[0].strip().encode(charset)
    sv1 = sv[1].strip().split(",")
    print "sv1: "
    print sv1
    #~ try:
        #~ sv1 = sv1.encode(charset)
    #~ except:
        #~ pass
    #~ try:
        #~ sv1 = int(sv1)
    #~ except:
        #~ pass 

    if len(sv) < 2:
        print "1.cannot extract stuff from srch_phrase: %s [ %s ]" % (s1, srch)
    else:
        if sv0 in vals:
            v = sv0
            print "v : %s" % v
            if v in scut:
                v = scut[v]
                
            for s in sv1:
                print "adding %s to v : %s" % (s, v)
                c_rule = "$in"
                s = s.strip()
                try:
                    if s[0] in ops:
                        if s[0] == "!":
                            c_rule = "$nin"
                        s = s[1:]
                except:
                    continue 
                try:
                    s = s.encode(charset)
                except:
                    pass 

                

                # doing regex-search 
                if s.find("*") > -1:
                    s = s.replace(".", "\.").replace("*", ".*")
                    c_rule = "$regex"
                    print "regex on %s" % s
                    sr = re.compile(s)
                    if v in sa:
                        if c_rule in sa[v]:
                            print "known regex/known v: %s / %s" % (v, s)
                            sa[v] = { "$all" : [sa[v][c_rule], sr], "$options": 'i'}
                        else:
                            print "new regex/know v: %s / %s" % (v, s)
                            sa[v] = { "%s" % c_rule: sr,  "$options": 'i'}
                            print sa[v][c_rule]
                            
                    else:
                        print "new regex/new v: %s / %s" % (v, s)
                        sa[v] = { "%s" % c_rule: sr,  "$options": 'i'}
                        print sa[v][c_rule]
                    
                    continue

                try:
                    s = int(s)
                except:
                    pass 


                
                if v in ["url", "zone"]:
                    c_rule = "$regex"
                    sa[v] = { "%s" % c_rule: "%s" % s.replace(".", "\.").replace("*", ".*"),  "$options": 'i'}
                    continue
                if v in sa:
                    print "2.v in sa: %s" % v
                    try:
                        print "c_rule is known, appending: %s" % s
                        f = sa[v][c_rule]
                        print f
                        f.append(s)
                        print f
                    except:
                        print "c_rule is new: %s" % c_rule
                        f = [s]
                    
                    sa[v][c_rule] = f
                else:
                    print "new v in sa: %s / %s" % (v, s)

#                        sa[v][c_rule] = [s]
                    sa[v] = { "%s" % c_rule: [s] }

                print "added rule_id to sa: %s" % s
                print sa
        else:
            print "unknown search-term: %s" % sv[0]
            sa = {}
            

#~ 
#~ 
    #~ print "op:"
    #~ print op
    #~ for o in op:
        #~ print "o: %s" % o
        #~ print op[o]
        #~ if len(op[o]) > 0:
            #~ print "O: %s" % o
            #~ if o == "or":
                #~ sa["$or"] = op[o]
            #~ elif o == "and":
                #~ sa["$and"] = op[o]
            #~ elif o == "not":
                #~ sa["$not"] = op[o]
            


    s1 = ""

    
    print sa
    
    print "tdiff: %s" % (smaller - bigger)

    c_data = [[], []]

    if cdata == 1:
        c_val = []
        c_desc = []
        xstep = 86400
        if (tf["stop"] - tf["start"]) > (86400 * 45): # more tahn 3weeks -> weekly summary
            xstep = (86400 * 7)
            
        so = {}
        for v in sa:
            so[v] = sa[v]
    
        if len(so) > 0:
            for xo in range(bigger, smaller, xstep):
                ccoll = db.events
                so["tstamp"] = { "$gte": xo, "$lt": (xo + xstep)}
                c_val.append(ccoll.find(so).count())
                c_desc.append(time.strftime("%m-%d", time.localtime(float(xo + xstep)))) 
    
        c_data = [c_desc, c_val]

    

    #~ sa = {}
    #~ sa["peer_ip"] = "64.39.111.95"


    
#    sa = {}
#    sa["$or"] = [{'peer_ip': '66.249.72.178'}, {'peer_ip': '220.76.58.47'}]
    #~ sa["peer_ip"] = { "$in": ['66.249.72.178', '220.76.58.47']}
    #~ sa["rule_id"] = { "$ne": 1000}



    print "final search_pattern:"
    print sa
    if len(sa) > 0:
        
        if cdata == 0:
            limit = 10000
        else:
            limit = d_limit
        res = coll.find(sa).sort("tstamp", DESC).skip(skip).limit(limit)
        s_l = []
        for s in res:
            sid = s["rule_id"]
            date = s["dstamp"]
            url = url_convert(s["url"])
            rip = s["peer_ip"].encode(charset)
            host = s["host"].encode(charset)
            mz = s["zone"].encode(charset)
            var_name = s["var_name"].encode(charset)
            content = s["content"].encode(charset)
            sensor = s["sensor_id"].encode(charset)
            # event_id for references
            eid     = s["_id"]
            
            try:
                s_msg = sigs_dict["%s" % sid][0]
            except:
                s_msg = "%s" % sid
    
            s_l.append((sid, s_msg, rip, url, date, host, mz, var_name, content, sensor, eid))
    

        u_ip = []
        u_sid = []
        u_host = []
        if cdata == 1:
            so = {}
            for v in sa:
                so[v] = sa[v]

            ru_ip = coll.find(sa).distinct("peer_ip")
        
            for ip in ru_ip:
                # get count for that ip
                so["peer_ip"] = { "$in": [ip] }
                cu_ip = coll.find(so).count()
                u_ip.append((ip, cu_ip))
        
        
            so = {}
            for v in sa:
                so[v] = sa[v]
            ru_sid = coll.find(so).distinct("rule_id")
            for sid in ru_sid:
                # get count for that ip
                so["rule_id"] = { "$in": [sid] }
                cu_sid = coll.find(so).count()
                u_sid.append((sid, cu_sid))
            
    
            so = {}
            for v in sa:
                so[v] = sa[v]
            ru_host = coll.find(so).distinct("host")
            for h in ru_host:
                # get count for that ip
                so["host"] = { "$in": [h] }
                cu_host = coll.find(so).count()
                u_host.append((h, cu_host))

            
        

    else:
        res = []
        s_l = []
        c_data = [[], []]
        u_ip = [(0,0)]
        u_sid = [(0, 0)]
        u_host = [(0,0)]

        

    print "s_l: %s" % len(s_l)
    #print s_l
    
    return(s_l, c_data, sorted(u_ip, reverse=True), sorted(u_sid, reverse=True), sorted(u_host, reverse=True))

        


def check_page(page):

    page_next = None 
    page_prev = None

    try:
        page = int(page)
    except:
        page = 0

    skip_step = app.config["DISPLAY_LIMIT"]

    if page < 1:
        skip = 0
        page = 0
        page_next = 1
    else:
        skip = page * skip_step
        page_next  = page + 1
        page_prev  = page - 1
        if page_prev < 1:
            page_prev  = " "

    return(page_next, page_prev, skip)
    

def sigs_parse(rulesets):

    
    global_sigs = {}
    
    for ruleset in rulesets:
        #print "checking %s" % ruleset
        sigs = parse_rulesfile(ruleset)
    
        
        for sig in sigs:
            global_sigs[sig] = sigs[sig]
            
    return(global_sigs)


def mdown(in_file):
    if not os.path.isfile(in_file):
        print "infile: %s" % in_file
        return(1000)
    
    
    
    
    # 'sane_lists', 'meta', 'extra', 'tables', 'fenced_code', 'codehilite'])
    ext = ['meta', 'extra', 'fenced_code', 'tables', 'codehilite', 'toc', 'attr_list']
    #md = markdown2.Markdown(extras=ext)
    #md = markdown2.Markdown(extras=ext)
    fxi = ""
    fx = "".join(open(in_file, "r").readlines()).decode(charset)
    
    #print fx
    fxout = markdown.markdown(fx, extensions=ext).replace("<img alt=", "<img id=\"blog_img\" alt=")
    #fxout = md.convert(fx).replace("<li>\n<p>", "<li>").replace("</p>\n</li>", "</li>").replace("<img alt=", "<img id=\"blog_img\" alt=")
    return(fxout)
    #return(markdown.markdown(fxi))

def mdown2(in_put):
    
    # 'sane_lists', 'meta', 'extra', 'tables', 'fenced_code', 'codehilite'])
    ext = ['meta', 'extra', 'fenced_code', 'tables', 'codehilite', 'toc', 'attr_list']
    #md = markdown2.Markdown(extras=ext)
    #md = markdown2.Markdown(extras=ext)
    fxi = ""
    fx = in_put
    
    #print fx
    fxout = markdown.markdown(fx, extensions=ext).replace("<img alt=", "<img id=\"blog_img\" alt=")
    #fxout = md.convert(fx).replace("<li>\n<p>", "<li>").replace("</p>\n</li>", "</li>").replace("<img alt=", "<img id=\"blog_img\" alt=")
    return(fxout)
    #return(markdown.markdown(fxi))


def render_content(in_put):
    """


    
    """
    basedir="pages"
    fp = "%s/%s.md" % (basedir, in_put)
    is_file = glob.glob(fp)
    if not is_file:
        print "cannot access %s" % fp
        resp = """
        <div align="center">
        <h1>404 - DONT PANIC</h1>
        the file you requested was not found
        <img src="/static/images/marvin4.jpg" width="70%">
        </div>
        """
        return(resp)
    else:
        
        rout = mdown("%s/%s.md" %  (basedir, in_put))
        return(rout)
    
    return("marvin is VERY tired")
