#!/usr/bin/python
#
# v2013-01-03


import sys


sys.path.append("lib/")
from doxi_lib import *

print """

DOXI - Installer
  docs: https://bitbucket.org/lazy_dogtown/doxi/overview

[+] creating local.rules 
"""

#~ f = open("doxi-rules/local.rules", "a")
#~ f.write("""
#~ #
#~ # local.rules for naxi - shall never be touched by rules-updates 
#~ #
#~ """)
#~ f.close()
#~ 
#~ print """
#~ 
#~ [+] copying rules
#~ 
#~ """

print """

> running initial download

"""

os.system("./dx-update -s")
os.system("./dx-update -n")
os.system("cp /etc/nginx/doxi-rules/rules.conf.template /etc/nginx/doxi-rules/rules.conf")

print """

> installing doxi-rules to nginx

"""
os.system("./dx-update -x")


