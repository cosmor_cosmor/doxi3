

<div align="left" markdown="1">
### DX-Console - Setup-Instructions 

DX_Console can be setup on actual running naxsi_hosts; you can run an inital
import onto older error.logs (see below) and have all your events displayed.

### Requirements

- nginx+naxsi
- python2.6ff + virtualenv
- mongodb @ console-host
- memcache @ console-host


### Definitions

- console-host: Host where your webinterface to naxsi_events (DX_Console) lives
- naxsi-host: Host that run nginx + naxsi and produce logfiles


#### First:

- read this whole manual
- define a host where your dx_console should reside; if you run a couple of naxsi-host you'd like to have a different host for that
- download doxi_tools from bitbucket to your workstation
- edit doxi-tools/contrib/nx_util_mod/nx_util.conf and adjust values (host, db, sensor_id etc)
- copy doxi-tools/contrib/nx_util_mod/ onto every naxsi-host

#### at console-host 

- steps as root:
    - install mongodb + memcached
    - adjust firewall-rules to have you naxsi-hosts access your mongodb
- download doxi_tools from bitbucket to your workstation
- initialize you console-env: 
    - this will download and install needed tools in a virtual enviroment, living in venv/
    - create an admin_user and admin_password; will be printed on the screen
~~~
cd doxi-tools && ./dx-console -c 
~~~

- edit web/dx_console.conf and adjust for your needs

- run ./dx-console -c again to start your console; it usually listens on localhost:5000, so putting
an nginx infront of it will 
- YMMV

- setup robot-cronjobs to receive messages about new uniq_stuff like new found urls, sigs or whatever attack-pattern; 
you should do it, since it othervise the webinterface may slow down, due to a lot of background_checks
that are excecuted from robot

~~~

*/5 * * * * cd /path/to/doxi-tools && ./dx-console -r 


~~~

- you'll need the robot to run later if you want to use agents to be alerted on certain findings, and to send emails to users




#### at naxsi-hosts

- Setup your working Naxsi-Installation
- copy all recent error.logs that contains into one file for inital parsing
- Setup nx_util_mod (needed for mongodb-imports, included in doxi-tools)
- run an intial check if the db-server can be reached:
~~~
cd /path/to/nx_util_mod && ./nx_util.py -x checkdb
~~~

- run your initial import:
~~~

cd /path/to/nx_util_mod && ./nx_util.py -c nx_util.conf -l /path/to/combined_error.log 

~~~

- setup a cronjob to tun nx_util:
~~~

*/7 * * * cd /path/to/nx_util_mod && ./nx_util.py -c nx_util.conf -l /path/to/nginx/error.log -i 

~~~

- Setup nx_util_modfied to 
