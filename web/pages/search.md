
<div align="left" markdown="1">

## Default - Search

<div class="row">
<div class="span2">
    <form action="/do/search/" method="POST">
    <input type="hidden" name="srch" value="ip = *">
    <input type="hidden" name="trange" value="24h" >
    <input type="submit"  class="btn btn-primary" value="24hrs - Events">
    </form>
</div>
<div class="span2">
    <form action="/do/search/" method="POST">
    <input type="hidden" name="srch" value="ip = *">
    <input type="hidden" name="trange" value="7d" >
    <input type="submit"  class="btn btn-primary" value="7 days - Events">
</form>
</div>
<div class="span2">
    <form action="/do/search/" method="POST">
    <input type="hidden" name="srch" value="ip = *">
    <input type="hidden" name="trange" value="30d" >
    <input type="submit"  class="btn btn-primary" value="30 days - Events">
</form>
</div>

</div>  

[Uniq IPs](/do/search/uniq__ip){:  class="btn btn-primary" }
[Uniq IDs](/do/search/uniq__id){:  class="btn btn-primary" }
[Uniq Urls](/do/search/uniq__url){:  class="btn btn-primary" }
[Uniq Hosts](/do/search/uniq__host){:  class="btn btn-primary" } 
[Uniq Var_Nmes](/do/search/uniq__var){:  class="btn btn-primary" } 
[Uniq MZ](/do/search/uniq__mz){:  class="btn btn-primary" } 
[Uniq Sensor_ID](/do/search/uniq__sensor_id){:  class="btn btn-primary" } 



</div>
