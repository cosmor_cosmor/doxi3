


<div align="left" markdown="1">
### DX-Console - Changelog



#### v0.4 - aug 2013 (desperatly seeking ono)

- 2nd alpha-preview
- distributed version, using multiple sensors
- only dashboard / sid / latest and search
- use [+] to add search_terms interactively from search_results
- /audex/ -> audit external ip w/out auth
- first test on whilelist-generation
- ip-reputation-lists (see features)
- mark messages as false-positives / delete from events_db 
- export search-results  / delete 
- /latest/new/ shows now summary for new events only, /latest/ shows summary of selected range


- bugfixes
    - msg_ack did not worked
    - search / ! or - did not got recognized
    - hmpf ... 

#### v0.3 - jul 2013 (summer frog release)

- first alpha-preview
- dashboard & charts
- search url | uniq et al
- search regexes
- search has now charts too
- combined host/ip/sensor-display as a search-result
- interface-msgs when new events are found
- sysmsgs when new hosts/urls/ips are found (robot)
- user-management (simple)
- reload_every_header (5 min) for dashboard/latest
- usersessions as dict{} in memcache
- nav-cleanup
- userlog on actions
- systemlog
- mark required fields in agent

- bugfixes
    - when sessions expires -> redirect to / -> /dashboard -> fixed; stays now on same page
    - updating user_session_time on every request
    - search & co now hast limit/skip + page, due to exploding, 900mb+  ram-eating processes when feeding live-results from sensors under attack 
    - grange_check fails
    - agent delete doesnt work
    - user_related stuff löike saved searches, fp_list or ack_messages should have uid instead of uname as 1st identifier


## bugs

- admin can overwrite existent user when creating new
- unknown val / typo in srch: sesnor_id = * 
- search uniq -> get from stats, independent from time_range
- msgs must have limit (200) + page, otherwise broken when > 1000 msgs 
- when deleting/fp make chunks of 10.000 and delete those 
- compare | hosts 
- check memcache / mongo at startup
- delete user_dir on user_del


## todo

- https://code.google.com/p/fuzzdb/


- checking user_names for spaces / non-alphanum-values
- when checking for objects from mongo_db, setting results in mc for 5 min
- when updating user_msg -> check if msg in admin/new_events
- checking if robot is running 
- dc-check-mode: feed a list of ips/cidrs and check if any of peer_ip is onto that list
- dashboard: have 24h/7d/30d mini-stats with min/max values for every part
- set page_reload off (when analysing a huge set of events)
- have reputation-ip-list in containers ??? -> yaml-style
- /admin/stats/ -> mongo-db-stats & mongo-console
- compare | host=x,y,z
- detail | host=x :  id=x,y,z

- user-defined shortcuts

- mongodb - congfig_cleanup

- app.config in own 



#### msgs

- ack_all


#### agents

- enable per user, must be admin to activate

- check_max
    - uniq_hosts
    - alerts(24h)
    - uniq_sids

- generic agents
    - check own ips -> feed in ip_list with own ips and check for 5min
    - check for x event oer 5 min per ip to find attackers fast
    - avg_based agent, checks for each given host the daily/hourly avg against the
        actual value, alerts on xxx percent to much
        - select: host/percent 
        
    - detector for avg hits/rules per ip and detect many hits/ip -> massive scan
    - templates
        - known vuln_scanners (zmeu, pohpmy, joomla, wp)
        - targeted_scan (detect manual scans by ua or signatures)
        - sqli / rfi etc
        - 


#### robot


#### msg-mails


#### in sig_display

- dash and graphs for scores


#### search



#### very advanced o.O

- have uniq_check from robot run for every host o.O







</div>
