$(function() {

    // Navigation
    $('a.nav-button').click(function(e) {
        $('nav ul:eq(0)').toggleClass('toggled');
        e.preventDefault();
    });

    // Navigation mit Unterpunkten?
    $('<span class="with-sub"></span>')
    	.appendTo('nav li:has(ul)')
    	.click(function(e) {
    		$(this)
    			.toggleClass('open')
    			.prev('ul')
    			.toggleClass('toggled');
    		e.preventDefault();
    	});

    // Suche
    $('a.search-button').click(function(e) {
        $('#search').toggleClass('toggled');
        e.preventDefault();
    });

});